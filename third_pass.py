"""
Third pass: fix Not Invented Here (NIH) syndrome. A lot
of wheels have been re-invented, badly.

1) Almost all of the functionality in this code
can be handled by a ModelSerializer and the ModelViewSet.
The only reason to override create() in a ModelViewSet is
if you want to add some specific fields (e.g. current user).
This does not appear to be the case here. We'll assume that
"comment" is included as a field for the user and included
with the serializer; why handle it separately, and risk
a KeyError if the field is missing?

Serialization of the response is likewise handled by the serializer
and default ViewSet endpoints, and there is no reason to change
these unless there is a specific reason to do so.

2) User permissions are handled already by DRF's permissions API.

Without further knowledge of the business logic I'll make these
assumptions:

1. Non-authenticated users can view user details
2. Authenticated users can add, edit and delete users.

The rules seem highly insecure (I can edit another user???) and
I'd flag them for further review, but that's out of scope here.

Given these assumptions I'd use the IsAuthenticatedOrReadOnly
permission which allows GET, OPTIONS and HEAD requests for
anonymous users and POST, DELETE etc i.e. write permissions on
all authenticated users. We can use more granular permissions
if needed later.

One further point: ensure DEFAULT_PAGINATION_CLASS is set
in the settings to prevent loading a huge result set into memory.

I'm assuming that's the case here.

"""

from django.contrib.auth import get_user_model

from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticatedOrReadOnly

from .serializers import UserSerializer

User = get_user_model()


class UsersViewSet(viewsets.ModelViewSet):
    serializer_class = UserSerializer
    queryset = User.objects.all()
    permission_classes = [IsAuthenticatedOrReadOnly]
