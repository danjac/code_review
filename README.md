Notes
-----

I've done this code review in three separate passes. A caveat is that this code is, of course, untested so I cannot guarantee there won't be any typos and other small mistakes here and there - this is just a high level review.
