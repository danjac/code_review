"""
Second pass: fix the imports
1. We're using *
2. Because of 1) we can't see which imports we might be missing
3. A number of unnecessary imports.

I'll assume there is a separate module for serializers.
"""

from django.contrib.auth import get_user_model

from rest_framework import viewsets
from rest_framework.response import Response

from .serializers import UserSerializer

# This is the recommended way of loading the user model
# as specified in settings.AUTH_USER_MODEL.
User = get_user_model()


class UsersViewSet(viewsets.ModelViewSet):
    serializer_class = UserSerializer
    queryset = User.objects.all()

    def create(self, request):
        if not request.user.is_authenticated():
            return Response("You should be authenticated")
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            comment = request.POST["comment"]
            user = User.objects.create(
                email=serializer.validated_data["email"],
                password=serializer.validated_data["password"],
                comment=comment,
            )
            response = {"code": 200, "success": True, "id": user.id}
            return Response(response)
        else:
            print("error on the creation")
            return Response("error")
